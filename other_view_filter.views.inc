<?php

/**
 * @file
 * Provide views data that related to other_view_filter module.
 */

/**
 * Implements hook_views_data_alter().
 */
function other_view_filter_views_data_alter(array &$data) {
  $data['node_field_data']['other_view_filter'] = [
    'title' => t('Use results of views'),
    'filter' => [
      'title' => t('Use results of views'),
      'help' => t('Select the view to use results as a filter criteria.'),
      'field' => 'nid',
      'id' => 'other_views_filter',
    ],
  ];
}
