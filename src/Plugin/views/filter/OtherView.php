<?php

namespace Drupal\other_view_filter\Plugin\views\filter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter content by user domain access field.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("other_views_filter")
 */
class OtherView extends InOperator {

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $viewStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $view_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->viewStorage = $view_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('view')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      'in' => [
        'title' => $this->t('Is one of'),
        'short' => $this->t('in'),
        'short_single' => $this->t('='),
        'method' => 'opSimple',
        'values' => 1,
      ],
      'not in' => [
        'title' => $this->t('Is not one of'),
        'short' => $this->t('not in'),
        'short_single' => $this->t('<>'),
        'method' => 'opSimple',
        'values' => 1,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $options = [];
      $views = $this->viewStorage->loadMultiple();
      foreach ($views as $view) {
        if ('node_field_data' !== $view->get('base_table') && 'nid' !== $view->get('base_field')) {
          continue;
        }
        $id = $view->id();
        // Return only views.
        foreach ($view->get('display') as $display) {
          $options[$id . ':' . $display['id']] = $this->t('View: @view - Display: @display', ['@view' => $id, '@display' => $display['id']]);
        }
      }

      $this->valueTitle = $this->t('Views');
      $this->valueOptions = $options;
    }
    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $this->getValueOptions();
    $default_value = $this->value;

    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('View to filter list'),
      '#default_value' => $default_value,
      '#description' => $this->t('Add a view to use results as a filter.'),
      '#options' => $this->valueOptions,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    if (empty($this->value) || !$view_results = $this->prepareView($this->value)) {
      return;
    }
    $this->ensureMyTable();

    $nids = [];
    foreach ($view_results as $row) {
      /** @var \Drupal\views\ResultRow $row */
      $nids[] = $row->nid;
    }
    $this->value = $nids;
    parent::opSimple();
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Filter in_operator expect array for validation.
    $this->value = [$this->value];
    return parent::validate();
  }

  /**
   * Checking the view.
   *
   * @param string $value
   *   The name of a view.
   *
   * @return bool|\Drupal\views\ViewExecutable
   *   Returned the view or FALSE.
   */
  protected function prepareView($value) {
    if (empty($value)) {
      return FALSE;
    }
    list($view_name, $display_id) = explode(':', $value);
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->viewStorage->load($view_name)->getExecutable();
    if (empty($view) || !$view->access($display_id)) {
      return FALSE;
    }
    $view->setDisplay($display_id);
    $view->preExecute();
    $view->execute();
    return !empty($view->result) ? $view->result : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['url']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeTags(parent::getCacheContexts(), ['url']);
  }

}
